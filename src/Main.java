
public class Main {
	
	public static void main(String[] args) {
		Library lib = new Library()  ;
		Member stu = new Member("Methanan Amornpipattananon","5610450292","D14")  ;
		Member tea = new Member("David De Gea","0","D14")   ;
		Member peo = new Member("Somchai Rukchart","0","Security Guard")   ;
		
		Book book1 = new Book("Big Java","2012")  ;
		Book book2 = new Book("PPL","2014")  ;
		Book book3 = new Book("Python","2002")  ;
		Book book4 = new Book("Software Analysis and Design","2010")  ;
		ReferenceBook refBook = new ReferenceBook("Java Reference","2010")  ;
		ReferenceBook refBook2 = new ReferenceBook("ThaiRath","2015")  ;
		ReferenceBook refBook3= new ReferenceBook("KomChadLuek","2015")  ;
		ReferenceBook refBook4 = new ReferenceBook("Python Reference","2012")  ;
		
		//แอดหนังสือ
		lib.addBook("Big Java",book1)  ;
		lib.addBook("PPL",book2)  ;
		lib.addBook("Python",book3)  ;
		lib.addBook("Software Analysis and Design",book4)   ;
		lib.addRefBook("Java Reference",refBook)  ;
		lib.addRefBook("ThaiRath",refBook2)  ;
		lib.addRefBook("KomChadLuek",refBook3)  ;
		lib.addRefBook("Python Reference",refBook4)  ;
		
		System.out.println(lib.getBookCount())  ; 
		//ยืม
		lib.borrowBook(stu,"Big Java")  ;  
		lib.borrowBook(tea,"PPL")  ;
		lib.borrowBook(stu,"Python")  ;
		lib.borrowBook(peo,"Software Analysis and Design")  ;
	
		lib.borrowBook(peo,"Java Reference")  ;
		lib.borrowBook(peo,"KomChadLuek")  ;
		lib.borrowBook(peo,"ThaiRath")  ;
		lib.borrowBook(peo,"Python Reference")  ; 
		System.out.println(lib.getBookCount())  ;
		
		//คืน
		lib.returnBook(tea,"PPL")  ;
		lib.returnBook(stu,"Big Java")  ;
		lib.returnBook(stu, "Python");
		lib.returnBook(peo, "Software Analysis and Design");
		
		
		System.out.println(lib.getBookCount())  ;
	}
}
